# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-09-19 01:37+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: /usr/lib/python2.4/optparse.py:332
#, python-format
msgid "usage: %s\n"
msgstr ""

#: /usr/lib/python2.4/optparse.py:351
msgid "Usage"
msgstr ""

#: /usr/lib/python2.4/optparse.py:357
msgid "integer"
msgstr ""

#: /usr/lib/python2.4/optparse.py:358
msgid "long integer"
msgstr ""

#: /usr/lib/python2.4/optparse.py:359
msgid "floating-point"
msgstr ""

#: /usr/lib/python2.4/optparse.py:360
msgid "complex"
msgstr ""

#: /usr/lib/python2.4/optparse.py:368
#, python-format
msgid "option %s: invalid %s value: %r"
msgstr ""

#: /usr/lib/python2.4/optparse.py:376
#, python-format
msgid "option %s: invalid choice: %r (choose from %s)"
msgstr ""

#: /usr/lib/python2.4/optparse.py:1141
msgid "show this help message and exit"
msgstr ""

#: /usr/lib/python2.4/optparse.py:1146
msgid "show program's version number and exit"
msgstr ""

#: /usr/lib/python2.4/optparse.py:1169
msgid "%prog [options]"
msgstr ""

#: /usr/lib/python2.4/optparse.py:1379 /usr/lib/python2.4/optparse.py:1418
#, python-format
msgid "%s option requires an argument"
msgstr ""

#: /usr/lib/python2.4/optparse.py:1381 /usr/lib/python2.4/optparse.py:1420
#, python-format
msgid "%s option requires %d arguments"
msgstr ""

#: /usr/lib/python2.4/optparse.py:1390
#, python-format
msgid "%s option does not take a value"
msgstr ""

#: /usr/lib/python2.4/optparse.py:1407 /usr/lib/python2.4/optparse.py:1561
#, python-format
msgid "no such option: %s"
msgstr ""

#: /usr/lib/python2.4/optparse.py:1507
msgid "options"
msgstr ""

#: /usr/lib/python2.4/optparse.py:1565
#, python-format
msgid "ambiguous option: %s (%s?)"
msgstr ""

#: pysdk-gui.py:83
msgid ""
"\n"
"  %prog [GUI Options] [Print Options] [PDF Options] [Image Options] [INPUT."
"sdk]\n"
"  %prog --version | -h | -m | -w\n"
"\n"
"  INPUT.sdk is a Python Sudoku file"
msgstr ""

#: pysdk-gui.py:92
msgid "show the modules not found"
msgstr ""

#: pysdk-gui.py:96 pysdk-image.py:92 pysdk-pdf.py:98 pysdk.py:100
msgid "information about what is sudoku"
msgstr ""

#: pysdk-gui.py:99 pysdk.py:103
msgid "Creation Options"
msgstr ""

#: pysdk-gui.py:103 pysdk.py:107
#, python-format
msgid ""
"set the difficulty of the sudoku (\"hard\", \"normal\", \"easy\") (\"%s\" is "
"the default)"
msgstr ""

#: pysdk-gui.py:107 pysdk.py:114
#, python-format
msgid ""
"set the handicap of the sudoku (0 = insane, 1 = insane + 1 extra number, "
"etc) (%d is the default)"
msgstr ""

#: pysdk-gui.py:111 pysdk.py:118
#, python-format
msgid ""
"set the region width. The board will be HxW grid of WxH grids (%d is the "
"default)"
msgstr ""

#: pysdk-gui.py:115 pysdk.py:122
#, python-format
msgid ""
"set the region height. The board will be HxW grid of WxH grids (%d is the "
"default)"
msgstr ""

#: pysdk-gui.py:119 pysdk-pdf.py:164
msgid "Print Options"
msgstr ""

#: pysdk-gui.py:121 pysdk-pdf.py:166
msgid "set the command to print (not value set)"
msgstr ""

#: pysdk-gui.py:122 pysdk-pdf.py:167
#, python-format
msgid "set the command to print (\"%s\" is the default)"
msgstr ""

#: pysdk-gui.py:130
msgid "Print and PDF Options"
msgstr ""

#: pysdk-gui.py:134 pysdk-pdf.py:105
#, python-format
msgid ""
"set the page size (\"A4\", \"LEGAL\", \"LETTER\", etc) (\"%s\" is the "
"default)"
msgstr ""

#: pysdk-gui.py:138 pysdk-pdf.py:109
msgid "don't draw the title text"
msgstr ""

#: pysdk-gui.py:142 pysdk-pdf.py:113
#, python-format
msgid "set the title font (\"%s\" is the default)"
msgstr ""

#: pysdk-gui.py:146 pysdk-pdf.py:117
#, python-format
msgid "set the title colour (\"black\", \"blue\", etc) (\"%s\" is the default)"
msgstr ""

#: pysdk-gui.py:150 pysdk-pdf.py:121
#, python-format
msgid "set the title font size (%d is the default)"
msgstr ""

#: pysdk-gui.py:154 pysdk-pdf.py:125
msgid "don't draw the filename"
msgstr ""

#: pysdk-gui.py:158 pysdk-pdf.py:129
#, python-format
msgid "set the filename font (\"%s\" is the default)"
msgstr ""

#: pysdk-gui.py:162 pysdk-pdf.py:133
#, python-format
msgid ""
"set the filename colour (\"black\", \"blue\", etc) (\"%s\" is the default)"
msgstr ""

#: pysdk-gui.py:166 pysdk-pdf.py:137
#, python-format
msgid "set the filename size (%d is the default)"
msgstr ""

#: pysdk-gui.py:169 pysdk-pdf.py:140
msgid "show 4 sudokus instead of 1"
msgstr ""

#: pysdk-gui.py:173 pysdk-pdf.py:144
msgid "show valids fonts"
msgstr ""

#: pysdk-gui.py:177 pysdk-image.py:95
msgid "Image Options"
msgstr ""

#: pysdk-gui.py:181 pysdk-image.py:99
#, python-format
msgid "set the image format (\"png\", \"jpeg\", etc) (\"%s\" is the default)"
msgstr ""

#: pysdk-gui.py:185 pysdk-image.py:103
#, python-format
msgid "set the image width in pixels (%d is the default)"
msgstr ""

#: pysdk-gui.py:189 pysdk-image.py:107
#, python-format
msgid "set the image height in pixels (%d is the default)"
msgstr ""

#: pysdk-gui.py:193 pysdk-image.py:111
msgid ""
"create a transparent image without background (if the format doesn't support "
"transparency a black background is created)"
msgstr ""

#: pysdk-gui.py:197 pysdk-image.py:115
#, python-format
msgid ""
"set the image background (\"white\", \"blue\", etc) (\"%s\" is the default)"
msgstr ""

#: pysdk-gui.py:202
msgid "Print, PDF and Image Options"
msgstr ""

#: pysdk-gui.py:207
#, python-format
msgid ""
"set the lines colour (\"black\", \"blue\", etc) (\"%s\" is the default for "
"Image and \"%s\" for PDF/Print)"
msgstr ""

#: pysdk-gui.py:211
#, python-format
msgid ""
"set the font for the numbers (absolute path or relative to the script) (\"%s"
"\" is the default for Image and \"%s\" for PDF/Print)"
msgstr ""

#: pysdk-gui.py:216
#, python-format
msgid ""
"set the font size for the numbers (%d is the default for image and %d for "
"PDF/Print)"
msgstr ""

#: pysdk-gui.py:221
#, python-format
msgid ""
"set the font colour for the numbers (\"black\", \"blue\", etc) (\"%s\" is "
"the default for Image and \"%s\" for PDF/Print)"
msgstr ""

#: pysdk-gui.py:224 pysdk-image.py:135 pysdk-pdf.py:174 pysdk.py:125
msgid "Visualization Options"
msgstr ""

#: pysdk-gui.py:226 pysdk-image.py:137 pysdk-pdf.py:176 pysdk.py:127
msgid "show letters instead of numbers > 9 (default)"
msgstr ""

#: pysdk-gui.py:227 pysdk-image.py:138 pysdk-pdf.py:177 pysdk.py:128
msgid "show only numbers"
msgstr ""

#: pysdk-gui.py:229 pysdk-image.py:140 pysdk-pdf.py:179 pysdk.py:130
msgid "show letters instead of numbers > 9"
msgstr ""

#: pysdk-gui.py:230 pysdk-image.py:141 pysdk-pdf.py:180 pysdk.py:131
msgid "show only numbers (default)"
msgstr ""

#: pysdk-gui.py:256 pysdk-image.py:167 pysdk-pdf.py:210 pysdk.py:159
msgid "incorrect number of arguments"
msgstr ""

#: pysdk-gui.py:342
msgid ""
"You have not psyco installed, if you can, install it to get better "
"performance"
msgstr ""

#: pysdk-gui.py:344
msgid "You have not reportlab installed, necessary to save as PDF and printing"
msgstr ""

#: pysdk-gui.py:346
msgid "You have not PIL installed, necessary to save as image"
msgstr ""

#: pysdk-gui.py:348
msgid "You have not pygtk installed, necessary to gui"
msgstr ""

#: pysdk-gui.py:365 pysdk-gui.py:367 pysdk-image.py:213 pysdk-image.py:215
#: pysdk-pdf.py:276 pysdk-pdf.py:278 pysdk.py:207 pysdk.py:209
msgid "\t-- cut here --"
msgstr ""

#: pysdk-gui.py:369 pysdk-image.py:217 pysdk-pdf.py:280 pysdk.py:211
#, python-format
msgid ""
"An error has happened, please go to %s and send a bug report with the last "
"lines."
msgstr ""

#: pysdk-image.py:82
msgid ""
"\n"
"  %prog [Image Options] INPUT.sdk OUTPUT.format\n"
"  %prog --version | -h | -m | -w\n"
"\n"
"  INPUT.sdk is a Python Sudoku file\n"
"  OUTPUT.format is an image file"
msgstr ""

#: pysdk-image.py:120 pysdk-pdf.py:149
#, python-format
msgid "set the lines colour (\"black\", \"blue\", etc) (\"%s\" is the default)"
msgstr ""

#: pysdk-image.py:124 pysdk-pdf.py:153
#, python-format
msgid ""
"set the font for the numbers (absolute path or relative to the script) (\"%s"
"\" is the default)"
msgstr ""

#: pysdk-image.py:128 pysdk-pdf.py:157
#, python-format
msgid "set the font size for the numbers (%d is the default)"
msgstr ""

#: pysdk-image.py:132 pysdk-pdf.py:161
#, python-format
msgid ""
"set the font colour for the numbers (\"black\", \"blue\", etc) (\"%s\" is "
"the default)"
msgstr ""

#: pysdk-pdf.py:84
msgid ""
"\n"
"  %prog [PDF Options] [-p [Print Options]] INPUT.sdk [INPUT.sdk INPUT.sdk "
"INPUT.sdk]\n"
"  %prog --version | -h | -m | -w\n"
"\n"
"  INPUT.sdk is a Python Sudoku file\n"
"  OUTPUT.pdf is a PDF file"
msgstr ""

#: pysdk-pdf.py:94
msgid "print a sudoku"
msgstr ""

#: pysdk-pdf.py:101
msgid "PDF Options"
msgstr ""

#: pysdk.py:81
msgid ""
"\n"
"  %prog INPUT.sdk [OUTPUT.sdk]\n"
"  %prog -c [Creation Options] OUTPUT.sdk\n"
"  %prog -t INPUT.sdk\n"
"  %prog --version | -h | -m | -w\n"
"\n"
"  INPUT.sdk and OUTPUT.sdk are Python Sudoku files"
msgstr ""

#: pysdk.py:92
msgid "create a sudoku"
msgstr ""

#: pysdk.py:96
msgid "test the difficulty of a sudoku"
msgstr ""

#: pysdk.py:110
msgid ""
"be sure that the difficulty of the sudoku created is the difficulty given"
msgstr ""

#: pythonsudoku/gui.py:49
msgid "_Open"
msgstr ""

#: pythonsudoku/gui.py:54
msgid "_Save sudoku"
msgstr ""

#: pythonsudoku/gui.py:60
msgid "Save as P_DF"
msgstr ""

#: pythonsudoku/gui.py:66
msgid "Save as _Image"
msgstr ""

#: pythonsudoku/gui.py:73
msgid "_Print"
msgstr ""

#: pythonsudoku/gui.py:78
msgid "_Quit"
msgstr ""

#: pythonsudoku/gui.py:83
msgid "_File"
msgstr ""

#: pythonsudoku/gui.py:92
msgid "_Undo"
msgstr ""

#: pythonsudoku/gui.py:97
msgid "_Redo"
msgstr ""

#: pythonsudoku/gui.py:102
msgid "_Edit"
msgstr ""

#: pythonsudoku/gui.py:111
msgid "_Create"
msgstr ""

#: pythonsudoku/gui.py:116
msgid "C_heck"
msgstr ""

#: pythonsudoku/gui.py:121
msgid "_Solve"
msgstr ""

#: pythonsudoku/gui.py:126
msgid "_Give one number"
msgstr ""

#: pythonsudoku/gui.py:131
msgid "_Sudoku"
msgstr ""

#: pythonsudoku/gui.py:140
msgid "_About"
msgstr ""

#: pythonsudoku/gui.py:145
msgid "_What is"
msgstr ""

#: pythonsudoku/gui.py:150
msgid "_Help"
msgstr ""

#: pythonsudoku/gui.py:173
msgid "Open file"
msgstr ""

#: pythonsudoku/gui.py:183
msgid "Save file"
msgstr ""

#: pythonsudoku/gui.py:193
msgid "Save file as PDF"
msgstr ""

#: pythonsudoku/gui.py:203
msgid "Save file as an image"
msgstr ""

#: pythonsudoku/gui.py:218
msgid "Select a number"
msgstr ""

#: pythonsudoku/gui.py:394
msgid "Create sudoku"
msgstr ""

#: pythonsudoku/gui.py:398
msgid "Select your handicap"
msgstr ""

#: pythonsudoku/gui.py:439 pythonsudoku/text.py:76
msgid "Creating sudoku..."
msgstr ""

#: pythonsudoku/gui.py:616
msgid "Solved!"
msgstr ""

#: pythonsudoku/gui.py:648
msgid "This sudoku can be solved."
msgstr ""

#: pythonsudoku/gui.py:650 pythonsudoku/gui.py:653
msgid "This sudoku can't be solved."
msgstr ""

#: pythonsudoku/info.py:18
msgid ""
"Sudoku, sometimes spelled Su Doku, is a placement puzzle, also known as "
"Number Place in the United States. The aim of the puzzle is to enter a "
"numeral from 1 through 9 in each cell of a grid, most frequently a 9x9 grid "
"made up of 3x3 subgrids (called \"regions\"), starting with various numerals "
"given in some cells (the \"givens\"). Each row, column and region must "
"contain only one instance of each numeral. Completing the puzzle requires "
"patience and logical ability. Its grid layout is reminiscent of other "
"newspaper puzzles like crosswords and chess problems. Sudoku initially "
"became popular in Japan in 1986 and attained international popularity in "
"2005.\n"
"\n"
"More information in http://en.wikipedia.org/wiki/Sudoku"
msgstr ""

#: pythonsudoku/printer.py:33
msgid "Print command not set"
msgstr ""

#: pythonsudoku/text.py:87
msgid "sudoku with wrong difficulty!"
msgstr ""

#: pythonsudoku/text.py:90 pythonsudoku/text.py:119
msgid "success!"
msgstr ""

#: pythonsudoku/text.py:112
msgid "Solving sudoku..."
msgstr ""

#: pythonsudoku/text.py:122
msgid "can't be solved!"
msgstr ""

#: pythonsudoku/text.py:138
msgid "The difficulty of the sudoku is..."
msgstr ""
